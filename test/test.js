const assert = require("assert");
const sinon = require("sinon");

const models = require('../models');
const services = require('../services');

const User = models.User;
const ToDoList = models.ToDoList;
const Item = models.Item;

const ToDoListService = services.ToDoListService;
const EmailService = services.EmailService;

// email: sebastien.souphron.esgi@gmail.com

describe("Test", () => {
    it("mocha working", () => {
        assert.equal(true, true);
    });
});

describe("User Test", () => {
    const user = new User("max@gmail.com", "maxime", "d'Harboullé", "maximeetjulienlesamis",1990);

    it('User email is valid', () => {
        const mock = sinon.mock(user);
        mock.expects('validateEmail')
            .once()
            .withArgs('max@gmail.com')
            .returns(true);

        assert.equal(user.validateEmail(user.email), true);

        mock.verify();
        mock.restore();
    })

    it('User is valid', () => {
        assert.equal(user.isValid(), true)
    })

    it('User email is not valid', () => {
        user.email = "maxdgmail.com";
        assert.equal(user.validateEmail(user.email), false)
    })

    it('User is not valid', () => {
        assert.equal(user.isValid(), false)
    })
});


describe("EmailService Test", () => {
    const user = new User("max@gmail.com", "maxime", "d'Harboullé", "maximeetjulienlesamis",1996);
    user.toDoList = ToDoListService.createToDoList();


    it('Send email to User when item is added', () => {
        const item = new Item("open the door to the dog","Hey this is fun",1592110400000);

        const mockUser = sinon.mock(EmailService);
        mockUser.expects('send')
            .once()
            .withArgs(user, item);

        user.toDoList = ToDoListService.addItemToDoList(user, item);

        mockUser.verify();
        mockUser.restore();
    });

    it('Can not send email, age limit of 18 years old', () => {
        const item = new Item("open the door to the dog again","Hey this is very fun",1592410400000);
        user.birthDate = 2005;

        const mockUser = sinon.mock(EmailService);
        mockUser.expects('send')
            .once()
            .withArgs(user, item);

        user.toDoList = ToDoListService.addItemToDoList(user, item);

        mockUser.verify();
        mockUser.restore();
    })

});

describe("ToDoListService Test", () => {

    //set up
    const user = new User("max@gmail.com", "maxime", "d'Harboullé", "maximeetjulienlesamis",1990);

    it("No ToDoList", () => {
        assert.equal(user.toDoList,null);
    });

    it("Create ToDoList", () => {
        user.toDoList = ToDoListService.createToDoList();
        assert.notEqual(user.toDoList,null);
    });

    it("Add two Item to toDoList with time difference over 30 minutes", () => {

        const item1 = new Item('bake',"bake a sour lemon cake",1590079500000);
        const item2 = new Item('find wallet',"i lost my wallet on st george street last night  :'(",1590081600000);

        user.toDoList = ToDoListService.addItemToDoList(user, item1);

        assert.equal(user.toDoList.items.length, 1);

        user.toDoList = ToDoListService.addItemToDoList(user, item2);

        assert.equal(user.toDoList.items.length, 2);

    });

    it("Add Item less than 30 minutes before last one isn't added", () => {

        const item3 = new Item('Go see grandParents',"Visit Grand Canyon with grandpa and grandma",1590081600000);

        user.toDoList = ToDoListService.addItemToDoList(user, item3);

        assert.equal(user.toDoList.items.length, 2);
    });

    it("Can't add Item with description over 1000 characters", () => {
        const item = new Item("try reaching over the limit of items","Add Item less than 30 minutes before ld Item less than 30 minutes before ld Item less than 30 minutes before ld Item less than 30 minutes before ld Item less than 30 minutes before ld Item less than 30 minutes before ld Item less than 30 minutes before ld Item less than 30 minutes before ld Item less than 30 minutes before last one isn't addedAdd Item less than 30 minutes before last one isn't addedAdd Item less than 30 minutes before last one isn't addedAdd Item less than 30 minutes before last one isn't addedAdd Item less than 30 minutes before last one isn't addedAdd Item less than 30 minutes before last one isn't addedAdd Item less than 30 minutes before last one isn't addedAdd Item less than 30 minutes before last one isn't addedAdd Item less than 30 minutes before last one isn't addedAdd Item less than 30 minutes before last one isn't addedAdd Item less than 30 minutes before last one isn't addedAdd Item less than 30 minutes before last one isn't addedAdd Item less than 30 minutes before last one isn't added",1592110400000);
        user.toDoList = ToDoListService.addItemToDoList(user, item);
        assert.equal(user.toDoList.items.length, 2);
    });
    it("Add Items up to 10 element limit", () => {
        let item;
        for (let i = 0, date = 1590085200000, name = "Go see grandParents"; i < 8; i++, date+=3600000, name += i) {
            item = new Item(name,"Visit Grand Canyon with grandpa and grandma",date);
            user.toDoList = ToDoListService.addItemToDoList(user, item);
        }
        assert.equal(user.toDoList.items.length, 10);
    });

    it("Can't Add 11th element, stays at 10", () => {
        const item = new Item("try reaching over the limit of items","Hey this is fun",1592110400000);
        user.toDoList = ToDoListService.addItemToDoList(user, item);
        assert.equal(user.toDoList.items.length, 10);
    });
});
