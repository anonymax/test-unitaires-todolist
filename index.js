console.log("Start app");
const models = require('./models');
const User = models.User;
const Item = models.Item;

const u1 = new User("max@gmail.com", "maxime", "d'Harboullé",1999, "12345678");
const item1 = new Item('work',"working",Date.now());
const item2 = new Item('work1',"working2",Date.now() + 1000 * 60 * 60);

console.log(item1);
console.log(u1.isValid());
