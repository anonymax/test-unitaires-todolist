class User {

    email;
    firstname;
    lastname;
    password;
    birthDate;
    toDoList;

    get email() {
        return this.email;
    }
    set email(value) {
        this.email = value;
    }
    get firstname() {
        return this.firstname;
    }
    set firstname(value) {
        this.firstname = value;
    }
    get lastname() {
        return this.lastname;
    }
    set lastname(value) {
        this.lastname = value;
    }
    get password() {
        return this.password;
    }
    set password(pwd) {
        this.password = pwd;
    }
    get birthDate() {
        return this.birthDate;
    }
    set birthDate(value) {
        this.birthDate = value;
    }
    get toDoList() {
        return this.toDoList;
    }
    set toDoList(toDoList) {
        this.toDoList = toDoList;
    }

    constructor(email, firstname, lastname, password, birthDate) {
        this.email = email;
        this.firstname = firstname;
        this.lastname = lastname;
        this.password = password;
        this.birthDate = birthDate;
        this.toDoList = null;
    }

    isValid() {
       if(this.firstname && this.lastname && this.firstname.length > 0
           && this.lastname.length > 0 && this.password.length >= 8
           && this.password.length <= 40 && new Date().getFullYear() - this.birthDate > 13
           && this.validateEmail(this.email)) {
           return true;
       }
       return false;
    }

    validateEmail(email) {
        let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }
}

module.exports = User;
