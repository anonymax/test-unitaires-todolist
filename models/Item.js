class Item {
    name;
    content;
    dateCreate;

    constructor(name, content, dateCreate = Date.now()) {
        this.name = name;
        this.content = content;
        this.dateCreate = dateCreate;
    }
}

module.exports = Item;
