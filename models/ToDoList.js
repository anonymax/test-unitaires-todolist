class ToDoList {

    items;

    constructor() {
        this.items = [];
    }

    canAddItem(newItem) {
        // itme lenght and content size check
        if(newItem.content.length > 1000 || this.items.length >= 10) {
            return null;
        }
        //unique name check
        this.items.forEach(item => {
            if(item.name === newItem.name){
                return null;
            }
        });
        // last added more than 30 minutes
        if(this.items.length > 0){
            if(this.dateDiffIsValid(newItem, this.items[this.items.length-1]))
            {
                return newItem;
            }
        }else{
            return newItem;
        }
        return null;
    }

    dateDiffIsValid(newItem, lastAddedItem){
        let diff = newItem.dateCreate - lastAddedItem.dateCreate;
        diff = diff/1000/60;
        if(diff > 30){
            return true;
        }
        return false;
    }

}

module.exports = ToDoList;
