const models = require('../models');
const ToDoList = models.ToDoList;

const EmailService = require("./EmailService");

class ToDoListService {

    static createToDoList(){
        return new ToDoList();
    }

    static addItemToDoList(user, item){
        const toDoList = user.toDoList;
        if(toDoList.canAddItem(item)) {
            toDoList.items.push(item);
            EmailService.send(user, item);
        }
        return toDoList;
    }

    static remove(toDoList, itemName){
        let index = -1;
        toDoList.items.forEach((item, i) => {
            if(item.name === itemName){
                index = i;
            }
        });
        toDoList.items.splice(i);
        return toDoList;
    }

    static deleteToDoList(){
        return [];
    }
}

module.exports = ToDoListService;
